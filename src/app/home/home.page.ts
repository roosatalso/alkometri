import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  private genders = [];
  private time = [];
  private bottles = [];
  private weight: number;
  private gender: string;
  private times: number;
  private bottle: number;
  private promilles: number;
  private litres: number;
  private burning: number;
  private grams: number;
  private gramsLeft: number;

  constructor() {}

  ngOnInit() {

    this.genders.push('Male');
    this.genders.push('Female');

    this.time.push(1);
    this.time.push(2);
    this.time.push(3);
    this.time.push(4);
    this.time.push(5);
    this.time.push(6);
    this.time.push(7);
    this.time.push(8);
    this.time.push(9);
    this.time.push(10);

    this.bottles.push(1);
    this.bottles.push(2);
    this.bottles.push(3);
    this.bottles.push(4);
    this.bottles.push(5);
    this.bottles.push(6);
    this.bottles.push(7);
    this.bottles.push(8);
    this.bottles.push(9);
    this.bottles.push(10);

  }

  calculate() {


  this.litres = this.bottle * 0.33;
  this.burning = this.weight / 10;
  this.grams = this.litres * 8 * 4.5;
  this.gramsLeft = this.grams - (this.burning * this.times);

  if (this.gender === 'Male') {
    this.promilles = this.gramsLeft / (this.weight * 0.7);
  } else {
    this.promilles = this.gramsLeft / (this.weight * 0.6);
  }
  
  }
  
}
